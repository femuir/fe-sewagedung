import { Component } from '@angular/core';

/**
 * Generated class for the TesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'tes',
  templateUrl: 'tes.html'
})
export class TesComponent {

  text: string;

  constructor() {
    console.log('Hello TesComponent Component');
    this.text = 'Hello World';
  }

}
