import { NgModule } from '@angular/core';
import { TesComponent } from './tes/tes';
@NgModule({
	declarations: [
		TesComponent],
	imports: [],
	exports: [
		TesComponent]
})
export class ComponentsModule { }
