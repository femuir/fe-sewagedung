import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, ToastController, Loading } from 'ionic-angular';
import { RequestListBuildingDto, ApiProvider } from '../../providers/api/api';
import { FormControl } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { debounceTime } from 'rxjs/operators';
import { DetailPage } from '../gedung/detail/detail';
import { Geolocation } from '@ionic-native/geolocation';
import { GlobalProvider } from '../../providers/global/global';
/**
 * Generated class for the HomeUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home-user',
  templateUrl: 'home-user.html',
})
export class HomeUserPage {
  user: any;
  fullName: string;
  data_building: any;
  rating: number = 4;
  loading: Loading;
  srcImg: any;
  search = new FormControl();
  totalPage: number = 1;
  httpError: boolean = false;
  locationError: boolean = false;
  formData: RequestListBuildingDto = {
    page: 1,
    limit: 5,
    search: '',
    price: 500000,
    rating: 1,
    capacity: 100,
    sortBy: 'distance',
    lat1: null,
    long1: null
  }
  countNotif: number = 0
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public apiProvider: ApiProvider,
    private geolocation: Geolocation,
    public globalProvider: GlobalProvider,
    public storage: Storage
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeUserPage');
    this.httpError = false;
    this.presentLoading();
    this.getBuilding();
    this.searchs();
    this.fullName = this.globalProvider.userName? this.globalProvider.userName : null;
  }

  getBuilding() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.formData.lat1 = resp.coords.latitude;
      this.formData.long1 = resp.coords.longitude;
      console.log('userLat', this.formData.lat1, 'userLong', this.formData.long1);
      this.apiProvider.listBuilding(this.formData)
        .subscribe((result: any) => {
          this.data_building = result.building.data;
          console.log('cek data', this.data_building);

          this.srcImg = this.apiProvider.urlImage + 'Building/';
          this.totalPage = result.building.totalPage;
          this.dismissLoading();
        }, error => {
          this.httpError = true;
          this.dismissLoading();
          this.showToast(error.error.msg);
        })
    }).catch((error) => {
      console.log('Error getting location', error);
      this.httpError = true;
      this.locationError = true;
      this.dismissLoading();
    });
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon tunggu...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  logRatingChange(rating) {
    console.log("changed rating: ", rating);
    // do your stuff
  }

  mFilter() {
    const modal = this.modalCtrl.create("FilterPage", { capacity: this.formData.capacity, price: this.formData.price }, {showBackdrop: true, enableBackdropDismiss: true});
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) {
        this.formData.page = 1;
        this.formData.capacity = data.capacity;
        this.formData.price = data.price;
        this.presentLoading();
        this.getBuilding();
      }
    })
  }

  mSort() {
    const modal = this.modalCtrl.create("ShortPage", { sortBy: this.formData.sortBy });
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) {
        this.formData.page = 1;
        this.formData.sortBy = data.sortBy;
        this.presentLoading();
        this.getBuilding();
      }
    })
  }

  mNotification() {
    const modal = this.modalCtrl.create("NotificationPage");
    modal.present();
  }

  searchs() {
    this.search.valueChanges.pipe(debounceTime(1500)).subscribe((keyword) => {
      this.formData.page = 1;
      this.formData.search = keyword;
      console.log(this.formData.search);

      this.getBuilding();
    })
  }

  goDetail(buildingId) {
    this.navCtrl.push(DetailPage, { buildingId });
  }

  doInfinite(infiniteScroll) {
    this.presentLoading();
    this.formData.page = this.formData.page + 1;
    this.apiProvider.listBuilding(this.formData)
      .subscribe((result: any) => {
        const data = result.building.data;
        if (data.length > 0) {
          for (let item of data) {
            this.data_building.push(item);
          }
        }
        infiniteScroll.complete();
        this.dismissLoading();
      }, error => {
        infiniteScroll.complete();
        this.dismissLoading();
      });
  }

  doRefresh(event) {
    // location.reload();
    this.ionViewDidLoad();
    setTimeout(() => {
      event.complete();
    }, 1000);
  }

}
