import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeUserPage } from './home-user';
import { StarRatingModule } from 'ionic3-star-rating';

@NgModule({
  declarations: [
    HomeUserPage,
  ],
  imports: [
    StarRatingModule,
    IonicPageModule.forChild(HomeUserPage),
  ],
})
export class HomeUserPageModule {}
