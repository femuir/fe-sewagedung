import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';

/**
 * Generated class for the HelpCenterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-help-center',
  templateUrl: 'help-center.html',
})
export class HelpCenterPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private callCelular: CallNumber
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpCenterPage');
  }

  openWA() {
    window.open('https://api.whatsapp.com/send?phone=6285693766474', '_system');
  }

  callNumber() {
    this.callCelular.callNumber("081212532456", true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

}
