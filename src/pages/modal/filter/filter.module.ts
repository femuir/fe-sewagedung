import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FilterPage } from './filter';
import { StarRatingModule } from 'ionic3-star-rating';

@NgModule({
  declarations: [
    FilterPage,
  ],
  imports: [
    StarRatingModule,
    IonicPageModule.forChild(FilterPage),
  ],
})
export class FilterPageModule {}
