import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the FilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {
  price: number = 500000;
  capacity: number = 100;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCrtl: ViewController
  ) {
    this.capacity = navParams.get('capacity') || this.capacity;
    this.price = navParams.get('price') || this.price;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterPage');
  }

  dismiss() {
    this.viewCrtl.dismiss();
  }

  doFilter() {
    this.viewCrtl.dismiss({ price: this.price, capacity: this.capacity });
  }

  doReset() {
    this.price = 500000;
    this.capacity = 100;
    this.viewCrtl.dismiss({ price: this.price, capacity: this.capacity });
  }

  logPrice() {
    console.log("price :", this.price)
  }
  logRatingChange(rating) {
    console.log("changed rating: ", rating);
  }

  closeModal(){
    this.viewCrtl.dismiss();
  }

}
