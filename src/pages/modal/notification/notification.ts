import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { GlobalProvider } from '../../../providers/global/global';
import { DetailPage } from '../../gedung/detail/detail';
import { ApiProvider } from '../../../providers/api/api';
import moment from 'moment';

/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {
  notification: any = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCrtl: ViewController,
    public globalProvider: GlobalProvider,
    public apiProvider: ApiProvider,
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
  }

  goNotification(notif) {
    console.log('cek1', notif);

    const trnId = notif.transactionId;
    const buildingId = notif.buildingId;

    if (notif.isTransaction == 1) {
      this.navCtrl.push("DetailTransactionPage", { trnId, buildingId })

    } else {
      this.navCtrl.push(DetailPage, { buildingId });
    }
    this.apiProvider.updateNotification(notif.id)
      .subscribe((result: any) => {
        console.log('getNotification', result.msg);

      }, error => {
        console.log('error update notification', error.error.msg);
      })
  }

  dismiss() {
    this.viewCrtl.dismiss();
  }

  formatDate(date) {
    const result = moment(date).format('DD-MM-YYYY');
    return result;
  }
}
