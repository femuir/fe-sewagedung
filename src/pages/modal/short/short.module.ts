import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShortPage } from './short';

@NgModule({
  declarations: [
    ShortPage,
  ],
  imports: [
    IonicPageModule.forChild(ShortPage),
  ],
})
export class ShortPageModule {}
