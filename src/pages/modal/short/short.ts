import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ShortPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-short',
  templateUrl: 'short.html',
})
export class ShortPage {
  sortBy:any = 'distance';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCrtl: ViewController
    ) {
      this.sortBy = navParams.get('sortBy') || this.sortBy;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShortPage');
  }

  applySorting(sorted){
    this.sortBy = sorted;
    // console.log(this.sort);
    // this.dismiss();
    this.viewCrtl.dismiss({ sortBy: this.sortBy});
    console.log(this.sortBy);
    
  }

  reset(){
    this.sortBy = 'lowPrice';
    this.dismiss();
  }

  dismiss() {
    this.viewCrtl.dismiss();
  }

}
