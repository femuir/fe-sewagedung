import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, ToastController, Loading, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RequestDetailTrxgDto, ApiProvider, RequestUpdateTrxgDto } from '../../../providers/api/api';
import { UploadPaymentPage } from '../upload-payment/upload-payment';
import { UserRateModalPage } from '../user-rate-modal/user-rate-modal';
import { ListTransactionPage } from '../list-transaction/list-transaction';
import { HomeManagementPage } from '../../management-building/home-management/home-management';
import moment from 'moment';
import { RejectTransactionPage } from '../../management-building/reject-transaction/reject-transaction';

/**
 * Generated class for the DetailTransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-transaction',
  templateUrl: 'detail-transaction.html',
})
export class DetailTransactionPage {
  detailTrx: any;
  dataBuilding: any;
  user: any;
  trnId: any;
  buildingId: any;
  loading: Loading;
  srcImage: any;
  formData: RequestDetailTrxgDto = {
    trnId: '',
    buildingId: ''
  }

  formUpdate: RequestUpdateTrxgDto = {
    trnId: '',
    status: '',
    notes: '',
    updatedBy: 'MANAGEMENT',
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public apiProvider: ApiProvider,
    public alertCtrl: AlertController
  ) {
    this.getUser();
    this.trnId = navParams.get('trnId');
    this.buildingId = navParams.get('buildingId');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailTransactionPage');
    this.getDetailTrx();
  }

  async getUser() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      console.log(user)
      console.log(this.user)

    }
    catch (error) {
      console.log(error)
    }
  }

  getDetailTrx() {
    this.formData.trnId = this.trnId;
    this.formData.buildingId = this.buildingId;
    this.presentLoading();
    this.apiProvider.detailTransaction(this.formData)
      .subscribe((result: any) => {
        this.detailTrx = result.transaction.detailTrx;
        this.dataBuilding = result.transaction.building;
        this.storage.set('building', JSON.stringify(result.transaction.building));
        this.srcImage = this.apiProvider.urlImage + 'Building/';
        this.dismissLoading();
        console.log(result.transaction.building);

      }, error => {
        this.dismissLoading();
        this.showToast(error.error.msg);
      })
  }

  goUploadPayment(trxId) {
    this.navCtrl.setRoot(UploadPaymentPage, { trxId });
  }

  goVerify(transactionId, proofOfPayment) {
    const modal = this.modalCtrl.create("VerifyPaymentPage", { payment: proofOfPayment, id: transactionId });
    modal.present();
  }

  verifyConfirm() {
    const confirm = this.alertCtrl.create({
      title: 'VERIVIKASI PEMBAYARAN',
      message: 'Apakah anda yakin data pembayaran sudah benar?',
      buttons: [
        {
          text: 'TIDAK',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YA',
          handler: () => {
            console.log('Agree clicked');
            this.doVerifyTrx();
          }
        }
      ]
    });
    confirm.present();
  }

  doVerifyTrx() {
    this.formUpdate.trnId = this.trnId;
    this.formUpdate.status = 'BOOKING';
    this.formUpdate.notes = this.detailTrx.notes;
    this.presentLoading();
    this.apiProvider.updateTransaction(this.formUpdate)
      .subscribe((result: any) => {
        this.dismissLoading();
        this.showToast(result.msg);
        this.goListTrx();
      }, error => {
        this.dismissLoading();
        this.showToast(error.error.msg);
      })
  }

  goRejectPage(transactionId) {
    // this.navCtrl.push("VerifyPaymentPage");   
    const modal = this.modalCtrl.create(RejectTransactionPage, { id: transactionId });
    modal.present();
  }

  checkinConfirm() {
    const confirm = this.alertCtrl.create({
      title: 'KONFIRMASI CHECKIN',
      message: 'Apakah anda yakin data transaksi diupdate menjadi CHECKIN?',
      buttons: [
        {
          text: 'TIDAK',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YA',
          handler: () => {
            console.log('Agree clicked');
            this.doCheckinTrx();
          }
        }
      ]
    });
    confirm.present();
  }

  doCheckinTrx() {
    this.formUpdate.trnId = this.trnId;
    this.formUpdate.status = 'CHECKIN';
    this.formUpdate.notes = this.detailTrx.notes;
    this.presentLoading();
    this.apiProvider.updateTransaction(this.formUpdate)
      .subscribe((result: any) => {
        this.dismissLoading();
        this.showToast(result.msg);
        this.goListTrx();
      }, error => {
        this.dismissLoading();
        this.showToast(error.error.msg);
      })
  }

  finishConfirm() {
    const confirm = this.alertCtrl.create({
      title: 'BOOKING SELESAI?',
      message: 'Apakah anda yakin Booking Gedung sudah selesai?',
      buttons: [
        {
          text: 'TIDAK',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YA',
          handler: () => {
            console.log('Agree clicked');
            this.doFinishTrx();
          }
        }
      ]
    });
    confirm.present();
  }

  doFinishTrx() {
    if (this.user.role == 2) {
      this.formUpdate.updatedBy = 'USER';
    }
    this.formUpdate.trnId = this.trnId;
    this.formUpdate.status = 'SELESAI';
    this.formUpdate.notes = this.detailTrx.notes;
    this.presentLoading();
    this.apiProvider.updateTransaction(this.formUpdate)
      .subscribe((result: any) => {
        this.dismissLoading();
        this.showToast(result.msg);
        if (this.user.role == 2) {
          this.goRate();
        } else {
          this.navCtrl.setRoot(HomeManagementPage);
        }
      }, error => {
        this.dismissLoading();
        this.showToast(error.error.msg);
      })
  }

  goRate() {
    const trnId = this.detailTrx.transactionId;
    const buildingId = this.detailTrx.buildingId;
    const userId = this.detailTrx.userId

    this.navCtrl.setRoot(UserRateModalPage, { trnId, buildingId, userId })
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon tunggu...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  goListTrx() {
    if (this.user.role == 2) {
      this.navCtrl.setRoot(ListTransactionPage);
    } else {
      this.navCtrl.setRoot(HomeManagementPage);
    }
  }

  formatDate(date) {
    const result = moment(date).format('DD-MM-YYYY');

    return result;
  }
}
