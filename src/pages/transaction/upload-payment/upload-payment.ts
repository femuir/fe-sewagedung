import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Loading, ActionSheetController } from 'ionic-angular';
import { ListTransactionPage } from '../list-transaction/list-transaction';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../../providers/api/api';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { GlobalProvider } from '../../../providers/global/global';

/**
 * Generated class for the UploadPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-upload-payment',
  templateUrl: 'upload-payment.html',
})
export class UploadPaymentPage {
  // data_transaction: any;
  building: any;
  loading: Loading;
  price: number;
  bank: string;
  accountName: string;
  accountNumber: string;
  trxId: any;
  imageData: any;
  imageDataBase64: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private camera: Camera,
    public globalProvider: GlobalProvider,
    public actionSheetCtrl: ActionSheetController,
    ) {      
      this.getBuilding();
      this.trxId = navParams.get('trxId');
      console.log(this.trxId);
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadPaymentPage');
  }

  async getBuilding() {
    try {
      const building = await this.storage.get('building');
      this.building = JSON.parse(building);
      this.bank = this.building.bank;
      this.accountNumber = this.building.accountNumber;
      this.accountName = this.building.accountName;
      this.price = this.building.price;
      // console.log(this.formData.buildingId);
    }
    catch (error) {
      console.log(error)
    }
  }

  optionUpload() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Profil Photo',
      buttons: [
        {
          text: 'Camera',
          role: 'destructive',
          icon: 'camera',
          handler: () => {
            this.takePicture(true);
            console.log('Destructive clicked');
          }
        }, {
          text: 'Galery',
          icon: 'image',
          handler: () => {
            this.takePicture(false);
            console.log('Archive clicked');
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(isCamera: boolean) {
    const options: CameraOptions = {
      quality: 20,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: isCamera ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.imageDataBase64 = 'data:image/jpeg;base64,' + imageData;
      console.log(this.imageDataBase64);
      this.imageData = imageData;
      console.log(this.imageData);
      this.uploadPhoto();
    }, (err) => {
      // Handle error
    });
  }

  async uploadPhoto() {
    const loading = this.loadingCtrl.create({ content: "Upload Bukti Pembayaran..."});
    // const userId = this.user.userId;
    const time = new Date().getTime().toString();
    const fileName = `${this.trxId + '_' + time}.jpg`
    console.log(fileName);
    loading.present();

    try {
      const image = await this.globalProvider.processFileFromUri(this.imageData);
      this.apiProvider.uploadPayment(image, this.trxId, fileName)
        .subscribe((result: any) => {
          console.log(result);
          loading.dismiss();
          this.showToast(result.msg);
          this.navCtrl.setRoot(ListTransactionPage)
        }, error => {
          console.log(error);
          loading.dismiss();
        })
    }
    catch (error) {
      console.log('çatch error', error);
      loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }
  
  uploadLater(){
    this.navCtrl.setRoot(ListTransactionPage);
  }

}
