import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadPaymentPage } from './upload-payment';

@NgModule({
  declarations: [
    UploadPaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(UploadPaymentPage),
  ],
})
export class UploadPaymentPageModule {}
