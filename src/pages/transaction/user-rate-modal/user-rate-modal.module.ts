import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserRateModalPage } from './user-rate-modal';
import { StarRatingModule } from 'ionic3-star-rating';

@NgModule({
  declarations: [
    UserRateModalPage,
  ],
  imports: [
    StarRatingModule,
    IonicPageModule.forChild(UserRateModalPage),
  ],
})
export class UserRateModalPageModule {}
