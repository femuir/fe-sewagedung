import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Loading, AlertController } from 'ionic-angular';
import { RequestInputRateDto, ApiProvider } from '../../../providers/api/api';
import { ListTransactionPage } from '../list-transaction/list-transaction';

/**
 * Generated class for the UserRateModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-rate-modal',
  templateUrl: 'user-rate-modal.html',
})
export class UserRateModalPage {
  trnId: any;
  buildingId: any;
  userId: any;
  comment: any;
  loading: Loading;

  formRate: RequestInputRateDto = {
    buildingId: '',
    userId: '',
    trnId: '',
    rate: 0,
    rated: 0,
    comment: ''
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController
  ) {
    this.trnId = navParams.get('trnId');
    this.buildingId = navParams.get('buildingId');
    this.userId = navParams.get('userId');
    console.log(this.trnId, this.buildingId, this.userId);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserRateModalPage');
  }

  logRatingChange(rating) {
    this.formRate.rate = rating;
    console.log("changed rating: ", this.formRate.rate);
  }

  // validationInput() {
  //   if (this.formRate.rate == 0) {
  //     const titleAlrt = "RATING TIDAK BOLEH KOSONG!";
  //     const subTitleAlrt = "Silahkan input rating untuk memberikan penilaian anda mengenai gedung ini";
  //     this.showAlert(titleAlrt, subTitleAlrt);

  //   } else if (!this.formRate.comment) {
  //     const titleAlrt = "REVIEW TIDAK BOLEH KOSONG!";
  //     const subTitleAlrt = "Silahkan input review untuk memberikan penilaian anda mengenai gedung ini";
  //     this.showAlert(titleAlrt, subTitleAlrt);
  //   } else if (this.formRate.rate && this.formRate.comment) {
  //     this.inputRateReview;
  //   }
  // }

  showAlert(titleAlrt, subTitleAlrt) {
    const alert = this.alertCtrl.create({
      title: titleAlrt,
      subTitle: subTitleAlrt,
      buttons: ['OK']
    });
    alert.present();
  }

  inputRateReview() {
    this.formRate.trnId = this.trnId;
    this.formRate.buildingId = this.buildingId;
    this.formRate.userId = this.userId;
    this.formRate.rated = 1;
    if (this.formRate.rate && this.formRate.comment) {
      this.presentLoading();
      this.apiProvider.inputRateReview(this.formRate)
        .subscribe((result: any) => {
          this.dismissLoading();
          this.showToast(result.msg);
          this.goListTrx();
        }, error => {
          this.dismissLoading();
          this.showToast(error.error.msg);
        })
    } else if (this.formRate.rate == 0) {
      const titleAlrt = "RATING TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan input rating untuk memberikan penilaian anda mengenai gedung ini";
      this.showAlert(titleAlrt, subTitleAlrt);

    } else {
      const titleAlrt = "REVIEW TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan input review untuk memberikan penilaian anda mengenai gedung ini";
      this.showAlert(titleAlrt, subTitleAlrt);
    }
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon tunggu...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  goListTrx() {
    this.navCtrl.setRoot(ListTransactionPage);
  }
}
