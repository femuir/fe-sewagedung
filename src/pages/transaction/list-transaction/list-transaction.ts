import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, ToastController } from 'ionic-angular';
import { RequestListTrxDto, ApiProvider } from '../../../providers/api/api';
import { Storage } from '@ionic/storage';
import moment from 'moment';
/**
 * Generated class for the ListTransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-transaction',
  templateUrl: 'list-transaction.html',
})
export class ListTransactionPage {
  transaction: string = "toPay";
  trxCheckout: any;
  trxPayment: any;
  trxBooking: any;
  trxFinish: any;
  totalPageCheckout: number;
  totalPagePayment: number;
  totalPageBooking: number;
  totalPageFinish: number;
  loading: Loading;
  user: any;
  imageData: any;
  formDataTrx: RequestListTrxDto = {
    page: 1,
    limit: 5,
    userId: '',
    buildingId: '',
    status1: '',
    status2: '',
    status3: '',
    status4: '',
    role: 2,
  }


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public storage: Storage,
  ) {
  }

  ionViewDidLoad() {
    this.presentLoading();
    console.log('ionViewDidLoad ListTransactionPage');
    this.getTrxCheckout();
    this.getTrxPayment();
    this.getTrxBooking();
    this.getTrxFinish();
    this.imageData = this.apiProvider.urlImage + 'Building/';
  }

  async getTrxCheckout() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      this.formDataTrx.userId = this.user.userId;

      this.formDataTrx.status1 = 'CHECKOUT';
      this.formDataTrx.status2 = '';
      this.apiProvider.listTransaction(this.formDataTrx)
        .subscribe((result: any) => {
          this.trxCheckout = result.transaction.data;
          this.totalPageCheckout = result.transaction.totalPage;
        }, error => {
          this.dismissLoading();
          this.showToast(error.error.msg);
        })
    }
    catch (error) {
      console.log(error);
    }
  }

  doInfiniteCheckout(infiniteScroll) {
    this.presentLoading();
    this.formDataTrx.status1 = 'CHECKOUT';
    this.formDataTrx.status2 = '';
    this.formDataTrx.page = this.formDataTrx.page + 1;
    this.apiProvider.listTransaction(this.formDataTrx)
      .subscribe((result: any) => {
        const data = result.transaction.data;
        if (data.length > 0) {
          for (let item of data) {
            this.trxCheckout.push(item);
          }
        }
        infiniteScroll.complete();
        this.dismissLoading();
      }, error => {
        infiniteScroll.complete();
      });
  }

  async getTrxPayment() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      this.formDataTrx.userId = this.user.userId;

      this.formDataTrx.status1 = 'DIBAYAR';
      this.formDataTrx.status2 = '';
      this.apiProvider.listTransaction(this.formDataTrx)
        .subscribe((result: any) => {
          this.trxPayment = result.transaction.data;
          this.totalPagePayment = result.transaction.totalPage;
        }, error => {
          this.dismissLoading();
          this.showToast(error.error.msg);
        })
    }
    catch (error) {
      console.log(error);
    }
  }

  doInfinitePayment(infiniteScroll) {
    this.presentLoading();
    this.formDataTrx.status1 = 'DIBAYAR';
    this.formDataTrx.status2 = '';
    this.formDataTrx.page = this.formDataTrx.page + 1;
    this.apiProvider.listTransaction(this.formDataTrx)
      .subscribe((result: any) => {
        const data = result.transaction.data;
        if (data.length > 0) {
          for (let item of data) {
            this.trxPayment.push(item);
          }
        }
        infiniteScroll.complete();
        this.dismissLoading();
      }, error => {
        infiniteScroll.complete();
      });
  }

  async getTrxBooking() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      this.formDataTrx.userId = this.user.userId;

      this.formDataTrx.status1 = 'BOOKING';
      this.formDataTrx.status2 = 'CHECKIN';
      this.apiProvider.listTransaction(this.formDataTrx)
        .subscribe((result: any) => {
          this.trxBooking = result.transaction.data;
          this.totalPageBooking = result.transaction.totalPage;
        }, error => {
          this.dismissLoading();
          this.showToast(error.error.msg);
        })
    }
    catch (error) {
      console.log(error);
    }
  }

  doInfiniteBooking(infiniteScroll) {
    this.presentLoading();
    this.formDataTrx.status1 = 'BOOKING';
    this.formDataTrx.status2 = 'CHECKIN';
    this.formDataTrx.page = this.formDataTrx.page + 1;
    this.apiProvider.listTransaction(this.formDataTrx)
      .subscribe((result: any) => {
        const data = result.transaction.data;
        if (data.length > 0) {
          for (let item of data) {
            this.trxBooking.push(item);
          }
        }
        infiniteScroll.complete();
        this.dismissLoading();
      }, error => {
        infiniteScroll.complete();
      });
  }

  async getTrxFinish() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      this.formDataTrx.userId = this.user.userId;

      this.formDataTrx.status1 = 'SELESAI';
      this.formDataTrx.status2 = 'DITOLAK';
      this.apiProvider.listTransaction(this.formDataTrx)
        .subscribe((result: any) => {
          this.trxFinish = result.transaction.data;
          this.totalPageFinish = result.transaction.totalPage;
          this.dismissLoading();
        }, error => {
          this.dismissLoading();
          this.showToast(error.error.msg);
        })
    }
    catch (error) {
      console.log(error);
    }
  }

  doInfiniteFinish(infiniteScroll) {
    this.presentLoading();
    this.formDataTrx.status1 = 'SELESAI';
    this.formDataTrx.status2 = 'DITOLAK';
    this.formDataTrx.page = this.formDataTrx.page + 1;
    this.apiProvider.listTransaction(this.formDataTrx)
      .subscribe((result: any) => {
        const data = result.transaction.data;
        if (data.length > 0) {
          for (let item of data) {
            this.trxFinish.push(item);
          }
        }
        infiniteScroll.complete();
        this.dismissLoading();
      }, error => {
        infiniteScroll.complete();
      });
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon tunggu...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  goDetail(trnId, buildingId) {
    this.navCtrl.push("DetailTransactionPage", { trnId, buildingId })
  }

  doRefresh(event) {
    this.ionViewDidLoad();
    setTimeout(() => {
      event.complete();
    }, 1000);
  }

  formatDate(date) {
    const result = moment(date).format('DD-MM-YYYY');

    return result;
  }
}
