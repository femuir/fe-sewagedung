import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListTransactionPage } from './list-transaction';

@NgModule({
  declarations: [
    ListTransactionPage,
  ],
  imports: [
    IonicPageModule.forChild(ListTransactionPage),
  ],
})
export class ListTransactionPageModule {}
