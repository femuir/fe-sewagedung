import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Loading, AlertController } from 'ionic-angular';
import { UploadPaymentPage } from '../upload-payment/upload-payment';
import { ApiProvider, RequestCheckoutTrxgDto } from '../../../providers/api/api';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the CheckoutTransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkout-transaction',
  templateUrl: 'checkout-transaction.html',
})
export class CheckoutTransactionPage {
  bookingDate: any;
  year: any;
  bookingTime: any = "09:00";
  building: any;
  buildingName: any;
  address: any;
  photoId: any;
  profil: any;
  srcImage: any;
  user: any;
  loading: Loading;
  formData: RequestCheckoutTrxgDto = {
    buildingId: '',
    userId: '',
    date: null,
    time: null,
    notes: ''
  }


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
  ) {
    this.getUser();
    // const date = new Date().toISOString();
    const date = new Date();
    const nextDate = date.setDate(date.getDate() + 1);
    const convertDate = new Date(nextDate).toISOString();
    console.log('getDate + 1:', convertDate);
    this.bookingDate = convertDate.split('T')[0];
    this.year = new Date().getFullYear();
    this.srcImage = this.apiProvider.urlImage + 'Building/';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutTransactionPage');
  }

  async getUser() {
    try {
      const user = await this.storage.get('user');
      const building = await this.storage.get('building');
      this.user = JSON.parse(user);
      this.building = JSON.parse(building);
      this.formData.userId = this.user.userId;
      this.formData.buildingId = this.building.buildingId;
      this.buildingName = this.building.buildingName;
      this.address = this.building.address;
      this.photoId = this.building.photoId;
      this.profil = this.building.profil;

      console.log('userId', this.formData.userId);
      console.log(this.formData.buildingId);
    }
    catch (error) {
      console.log(error)
    }
  }

  doCheckout() {
    this.formData.date = this.bookingDate,
    this.formData.time = this.bookingTime,
    this.presentLoading();
    this.apiProvider.checkoutTransaction(this.formData)
      .subscribe((result: any) => {
        this.dismissLoading();
        this.showToast(result.msg);
        this.goPayment(result.trx);
      }, error => {
        this.dismissLoading();
        this.showAlert(null, error.error.msg);
      })
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon tunggu...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  showAlert(titleAlrt, subTitleAlrt) {
    const alert = this.alertCtrl.create({
      title: titleAlrt,
      subTitle: subTitleAlrt,
      buttons: ['OK']
    });
    alert.present();
  }

  goPayment(trxId) {
    this.navCtrl.setRoot(UploadPaymentPage, {trxId});
  }

}
