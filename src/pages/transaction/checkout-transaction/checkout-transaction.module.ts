import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckoutTransactionPage } from './checkout-transaction';

@NgModule({
  declarations: [
    CheckoutTransactionPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckoutTransactionPage),
  ],
})
export class CheckoutTransactionPageModule {}
