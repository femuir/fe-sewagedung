import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { RequestRegisterUserDto, ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { GlobalProvider } from '../../providers/global/global';
import { HomeUserPage } from '../home-user/home-user';
import { LoginPage } from '../login/login';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})


export class RegisterPage {
  tNc: boolean = false;

  formData: RequestRegisterUserDto = {
    userId: '',
    fullName: '',
    gender: '',
    phone: '',
    address: '',
    role: 2,
    email: '',
    password: '',
    confirm_password: ''
  }

  loading: Loading;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public storage: Storage,
    public globalProvider: GlobalProvider,
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
    console.log(this.tNc);
  }

  doRegist() {
    //validation input
    if (!this.formData.userId) {
      const titleAlrt = "NIK TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan NIK anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.fullName) {
      const titleAlrt = "NAMA  TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan nama anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.email) {
      const titleAlrt = "EMAIL  TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan email anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.gender) {
      const titleAlrt = "GENDER  TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan pilih gender anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.address) {
      const titleAlrt = "ADDRESS  TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan alamat anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.phone) {
      const titleAlrt = "PHONE NUMBER  TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan no. HP anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.password) {
      const titleAlrt = "PASSWORD TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan password anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.confirm_password) {
      const titleAlrt = "CONFIRM PASSWORD TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan konfirmasi password anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else {
      this.presentLoading();
      this.apiProvider.registerUser(this.formData)
        .subscribe((result: any) => {
          this.setUserStorage(result);
          this.globalProvider.eventLogin.emit(result);
          this.goLogin();
          this.dismissLoading();
          this.showToast(result.msg);
        }, error => {
          this.dismissLoading();
          this.showAlert(error.error.msg, null);
        })
    }
  }

  async setUserStorage(result) {
    try {
      const user = await this.storage.set('user', JSON.stringify(result.user));
      console.log('set user:', user)
    }
    catch (error) {
      console.log(error)
    }
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon tunggu...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  showAlert(titleAlrt, subTitleAlrt) {
    const alert = this.alertCtrl.create({
      title: titleAlrt,
      subTitle: subTitleAlrt,
      buttons: ['OK']
    });
    alert.present();
  }

  goHome() {
    this.navCtrl.setRoot(HomeUserPage)
  }
  
  goLogin() {
    this.navCtrl.setRoot(LoginPage)
  }

  goRegistManagement() {
    this.navCtrl.push("RegisterManagementPage")
  }
}
