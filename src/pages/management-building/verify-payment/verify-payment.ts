import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ApiProvider } from '../../../providers/api/api';

/**
 * Generated class for the VerifyPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verify-payment',
  templateUrl: 'verify-payment.html',
})
export class VerifyPaymentPage {
  proofPayment: any;
  id: any;
  srcImage: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCrtl: ViewController,
    public apiProvider: ApiProvider,
    ) {
      // this.proofPayment = 'assets/imgs/proof of payment.png'
      this.proofPayment = navParams.get('payment');
      this.id = navParams.get('id');
      console.log(this.proofPayment);
      
      this.srcImage = this.apiProvider.urlImage + 'Transaction/';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyPaymentPage');
  }

  dismiss() {
    this.viewCrtl.dismiss();
  }

}
