import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeManagementPage } from './home-management';

@NgModule({
  declarations: [
    HomeManagementPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeManagementPage),
  ],
})
export class HomeManagementPageModule {}
