import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { RequestListTrxDto, ApiProvider } from '../../../providers/api/api';
import { Storage } from '@ionic/storage';
import { GlobalProvider } from '../../../providers/global/global';
import moment from 'moment';

/**
 * Generated class for the HomeManagementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home-management',
  templateUrl: 'home-management.html',
})
export class HomeManagementPage {
  fullName: string;
  transaction: string = "payment";
  trxCheckin: any;
  trxPayment: any;
  trxBooking: any;
  trxFinish: any;
  totalPageCheckin: number;
  totalPagePayment: number;
  totalPageBooking: number;
  totalPageFinish: number;
  loading: Loading;
  user: any;
  imageData: any;
  httpError: boolean = false;
  formDataTrx: RequestListTrxDto = {
    page: 1,
    limit: 5,
    userId: '',
    buildingId: '',
    status1: '',
    status2: '',
    status3: '',
    status4: '',
    role: 1,
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public storage: Storage,
    public globalProvider: GlobalProvider,
    public modalCtrl: ModalController,
  ) {
  }

  ionViewDidLoad() {
    this.httpError = false;
    console.log('ionViewDidLoad HomeManagementPage');
    this.fullName = this.globalProvider.userName? this.globalProvider.userName : null;
    this.httpError = false;
    this.presentLoading();
    this.getTrxPayment();
    this.getTrxBooking();
    this.getTrxCheckin();
    this.getTrxFinish();
    this.imageData = this.apiProvider.urlImage + 'User/';
  }

  mNotification() {
    const modal = this.modalCtrl.create("NotificationPage");
    modal.present();
  }

  async getTrxPayment() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      this.formDataTrx.buildingId = this.user.buildingId;

      this.formDataTrx.status1 = 'DIBAYAR';
      this.formDataTrx.status2 = '';
      this.apiProvider.listTransaction(this.formDataTrx)
        .subscribe((result: any) => {
          this.trxPayment = result.transaction.data;
          this.totalPagePayment = result.transaction.totalPage;
        }, error => {
          this.httpError = true;
          this.dismissLoading();
          this.showToast(error.error.msg);
        })
    }
    catch (error) {
      this.httpError = true;
      console.log(error);
    }
  }

  doInfinitePayment(infiniteScroll) {
    this.presentLoading();
    this.formDataTrx.status1 = 'DIBAYAR';
    this.formDataTrx.status2 = '';
    this.formDataTrx.page = this.formDataTrx.page + 1;
    this.apiProvider.listTransaction(this.formDataTrx)
      .subscribe((result: any) => {
        const data = result.transaction.data;
        if (data.length > 0) {
          for (let item of data) {
            this.trxPayment.push(item);
          }
        }
        infiniteScroll.complete();
        this.dismissLoading();
      }, error => {
        infiniteScroll.complete();
      });
  }

  async getTrxBooking() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      this.formDataTrx.buildingId = this.user.buildingId;

      this.formDataTrx.status1 = 'BOOKING';
      this.formDataTrx.status2 = '';
      this.apiProvider.listTransaction(this.formDataTrx)
        .subscribe((result: any) => {
          this.trxBooking = result.transaction.data;
          this.totalPageBooking = result.transaction.totalPage;
        }, error => {
          this.dismissLoading();
          this.httpError = true;
          this.showToast(error.error.msg);
        })
    }
    catch (error) {
      this.httpError = true;
      console.log(error);
    }
  }

  doInfiniteBooking(infiniteScroll) {
    this.presentLoading();
    this.formDataTrx.status1 = 'BOOKING';
    this.formDataTrx.status2 = '';
    this.formDataTrx.page = this.formDataTrx.page + 1;
    this.apiProvider.listTransaction(this.formDataTrx)
      .subscribe((result: any) => {
        const data = result.transaction.data;
        if (data.length > 0) {
          for (let item of data) {
            this.trxBooking.push(item);
          }
        }
        infiniteScroll.complete();
        this.dismissLoading();
      }, error => {
        infiniteScroll.complete();
      });
  }

  async getTrxCheckin() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      this.formDataTrx.buildingId = this.user.buildingId;

      this.formDataTrx.status1 = 'CHECKIN';
      this.formDataTrx.status2 = '';
      this.apiProvider.listTransaction(this.formDataTrx)
        .subscribe((result: any) => {
          this.trxCheckin = result.transaction.data;
          this.totalPageCheckin = result.transaction.totalPage;
        }, error => {
          this.httpError = true;
          this.dismissLoading();
          this.showToast(error.error.msg);
        })
    }
    catch (error) {
      this.httpError = true;
      console.log(error);
    }
  }

  doInfiniteCheckin(infiniteScroll) {
    this.presentLoading();
    this.formDataTrx.status1 = 'CHECKIN';
    this.formDataTrx.status2 = '';
    this.formDataTrx.page = this.formDataTrx.page + 1;
    this.apiProvider.listTransaction(this.formDataTrx)
      .subscribe((result: any) => {
        const data = result.transaction.data;
        if (data.length > 0) {
          for (let item of data) {
            this.trxCheckin.push(item);
          }
        }
        infiniteScroll.complete();
        this.dismissLoading();
      }, error => {
        infiniteScroll.complete();
      });
  }

  async getTrxFinish() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      this.formDataTrx.buildingId = this.user.buildingId;

      this.formDataTrx.status1 = 'SELESAI';
      this.formDataTrx.status2 = 'DITOLAK';
      this.apiProvider.listTransaction(this.formDataTrx)
        .subscribe((result: any) => {
          this.trxFinish = result.transaction.data;
          this.totalPageFinish = result.transaction.totalPage;
          this.dismissLoading();
        }, error => {
          this.httpError = true;
          this.dismissLoading();
          this.showToast(error.error.msg);
        })
    }
    catch (error) {
      this.httpError = true;
      console.log(error);
    }
  }

  doInfiniteFinish(infiniteScroll) {
    this.presentLoading();
    this.formDataTrx.status1 = 'SELESAI';
    this.formDataTrx.status2 = 'DITOLAK';
    this.formDataTrx.page = this.formDataTrx.page + 1;
    this.apiProvider.listTransaction(this.formDataTrx)
      .subscribe((result: any) => {
        const data = result.transaction.data;
        if (data.length > 0) {
          for (let item of data) {
            this.trxFinish.push(item);
          }
        }
        infiniteScroll.complete();
        this.dismissLoading();
      }, error => {
        infiniteScroll.complete();
      });
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon tunggu...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  goDetail(trnId, buildingId) {
    this.navCtrl.push("DetailTransactionPage", { trnId, buildingId })
  }

  doRefresh(event) {
    this.ionViewDidLoad();
    setTimeout(() => {
      event.complete();
    }, 1000);
  }

  formatDate(date) {
    const result = moment(date).format('DD-MM-YYYY');
    return result;
  }
}
