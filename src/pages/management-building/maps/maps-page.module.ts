import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapsPage } from './maps-page';
import { ComponentsModule } from '../../../components/components.module';
import { MapsComponentModule } from '../../../components/maps-component/maps-component.module';

@NgModule({
  declarations: [
    MapsPage,
  ],
  imports: [
    IonicPageModule.forChild(MapsPage),
    MapsComponentModule,
    ComponentsModule
  ],
})
export class MapsPageModule {}
