import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RejectTransactionPage } from './reject-transaction';

@NgModule({
  declarations: [
    // RejectTransactionPage,
  ],
  imports: [
    IonicPageModule.forChild(RejectTransactionPage),
  ],
})
export class RejectTransactionPageModule {}
