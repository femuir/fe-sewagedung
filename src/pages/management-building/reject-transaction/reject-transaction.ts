import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController, Loading, AlertController } from 'ionic-angular';
import { ApiProvider, RequestUpdateTrxgDto } from '../../../providers/api/api';
import { HomeManagementPage } from '../home-management/home-management';

/**
 * Generated class for the RejectTransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reject-transaction',
  templateUrl: 'reject-transaction.html',
})
export class RejectTransactionPage {
  trnId: any;
  loading: Loading;
  formUpdate: RequestUpdateTrxgDto = {
    trnId: '',
    status: '',
    notes: '',
    updatedBy: 'MANAGEMENT'
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCrtl: ViewController,
    public apiProvider: ApiProvider,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController
  ) {
    this.trnId = navParams.get('id');
    console.log('trnId', this.trnId);
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RejectTransactionPage');
  }

  rejectConfirm() {
    const confirm = this.alertCtrl.create({
      title: 'REJECT TRANSAKSI',
      message: 'Apakah anda yakin akan mereject transaksi ini?',
      buttons: [
        {
          text: 'TIDAK',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'YA',
          handler: () => {
            console.log('Agree clicked');
            this.doRejectTrx();
          }
        }
      ]
    });
    confirm.present();
  }

  doRejectTrx() {
    this.formUpdate.trnId = this.trnId;
    this.formUpdate.status = 'DITOLAK';
    if (this.formUpdate.notes) {
      this.presentLoading();
    this.apiProvider.updateTransaction(this.formUpdate)
      .subscribe((result: any) => {
        this.dismissLoading();
        this.showToast(result.msg);
        this.goListTrx();
      }, error => {
        this.dismissLoading();
        this.showToast(error.error.msg);
      })
    } else {
      const titleAlrt = "ALASAN TRANSAKSI DITOLAK HARUS DI ISI!";
      const subTitleAlrt = "Silahkan input alasan transaksi ini ditolak";
      this.showAlert(titleAlrt, subTitleAlrt);
    }
  }

  showAlert(titleAlrt, subTitleAlrt) {
    const alert = this.alertCtrl.create({
      title: titleAlrt,
      subTitle: subTitleAlrt,
      buttons: ['OK']
    });
    alert.present();
  }

  goListTrx() {
    this.navCtrl.setRoot(HomeManagementPage);
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon tunggu...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  dismiss() {
    this.viewCrtl.dismiss();
  }

}
