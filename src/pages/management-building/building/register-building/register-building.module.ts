import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterBuildingPage } from './register-building';

@NgModule({
  declarations: [
    RegisterBuildingPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterBuildingPage),
  ],
})
export class RegisterBuildingPageModule {}
