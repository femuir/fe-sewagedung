import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, ToastController, AlertController, ActionSheetController, ModalController, Content } from 'ionic-angular';
import { ApiProvider, RequestRegisterBuildingDto } from '../../../../providers/api/api';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { GlobalProvider } from '../../../../providers/global/global';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../../../login/login';

/**
 * Generated class for the RegisterBuildingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-building',
  templateUrl: 'register-building.html',
})
export class RegisterBuildingPage {
  @ViewChild(Content) content: Content;
  building = 'detail';
  imageData: any;
  imageDataBase64: any;
  imageData_1: any;
  imageDataBase64_1: any;
  imageData_2: any;
  imageDataBase64_2: any;
  imageData_3: any;
  imageDataBase64_3: any;
  imageData_4: any;
  imageDataBase64_4: any;
  tNc: boolean = false;
  photos: Array<{ key: string, file: any }> = [];
  user: any;
  // lat: any;
  // long: any;
  // addressBuilding: any;

  formData: RequestRegisterBuildingDto = {
    buildingId: '',
    buildingName: '',
    address: '',
    phone: '',
    capacity: '',
    price: '',
    moreInfo: '',
    status: 'A',
    managementId: '',
    city: '',
    bank: '',
    accountNumber: '',
    accountName: '',
    f1: '',
    f2: '',
    f3: '',
    f4: '',
    f5: '',
    f6: '',
    f7: '',
    f8: '',
    f9: '',
    f10: '',
    latitude: '',
    longtitude: ''
  }

  loading: Loading;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    public globalProvider: GlobalProvider,
    public storage: Storage,
    public modalCtrl: ModalController,
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterBuildingPage');
    console.log(this.tNc);
    this.getUserId();
  }

  test() {
    console.log(this.tNc);
  }

  async getUserId() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      console.log('user cek:', this.user);
    }
    catch (error) {
      console.log(error)
    }
  }

  doRegist() {
    //validation input
    if (!this.formData.buildingName) {
      const titleAlrt = "Building Name TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan Nama Gedung anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.address) {
      const titleAlrt = "Address TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan Alamat Gedung anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.phone) {
      const titleAlrt = "Phone TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan nomor telpon Gedung anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.capacity) {
      const titleAlrt = "Capacity TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan kapasitas Gedung anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.price) {
      const titleAlrt = "Price TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan harga Gedung anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.imageData) {
      const titleAlrt = "Photo Profile TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan upload photo profile Gedung anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.f1) {
      const titleAlrt = "Fasilitas 1 - 3 TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan fasilitas Gedung anda, minimal fasilitas 1 - 3";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.f2) {
      const titleAlrt = "Fasilitas 1 - 3 TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan fasilitas Gedung anda, minimal fasilitas 1 - 3";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.f3) {
      const titleAlrt = "Fasilitas 1 - 3 TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan fasilitas Gedung anda, minimal fasilitas 1 - 3";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.bank) {
      const titleAlrt = "Bank Name TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan Nama Bank untuk proses pembayaran gedung anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.accountNumber) {
      const titleAlrt = "Account Number TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan Nomor Rekening Bank untuk proses pembayaran gedung anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.accountName) {
      const titleAlrt = "Account Name TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan Nama Pemilik Rekening Bank untuk proses pembayaran gedung anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else {
      this.formData.managementId = this.user.userId;
      this.presentLoading(null);
      this.apiProvider.registerBuilding(this.formData)
        .subscribe(async (result: any) => {
          console.log(result);
          let temp = this.user;
          temp.buildingId = result.buildingId;
          this.storage.set('user', JSON.stringify(temp))
          console.log('temp', temp)
          this.dismissLoading();

          try {
            this.presentLoading('Uploading Image');
            await this.uploadMultiple(result.photoId);
            this.dismissLoading();
            this.goLogin();
            this.showToast(result.msg);

          }
          catch (error) {
            console.log(error);
            this.dismissLoading();
            this.showToast(error.error.msg);
          }
        }, error => {
          this.dismissLoading();
          // this.showAlert(error.error.msg);
          this.showToast(error.error.msg);
        })
    }
  }

  async uploadMultiple(photoId) {
    return new Promise(async (resolve, reject) => {
      if (this.photos.length > 0) {
        try {
          for (let item of this.photos) {
            const filename = new Date().getTime().toString() + '_' + photoId + '.jpg';
            const image = await this.globalProvider.processFileFromUri(item.file);
            await this.upload(image, photoId, item.key, filename);
          }

          resolve(true);
        }
        catch (error) {
          reject(error);
        }

      }
    })
  }

  upload(file, photoId, key, filename) {
    return new Promise((resolve, reject) => {
      this.apiProvider.uploadPhotoBuilding(file, photoId, key, filename)
        .subscribe((result) => {
          resolve(result);
        }, error => {
          reject(error);
        })
    })
  }

  presentLoading(msg) {
    if (msg == null) {
      msg = "Mohon tunggu..."
    }
    this.loading = this.loadingCtrl.create({
      content: msg,
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  showAlert(titleAlrt, subTitleAlrt) {
    const alert = this.alertCtrl.create({
      title: titleAlrt,
      subTitle: subTitleAlrt,
      buttons: ['OK']
    });
    alert.present();
  }


  goLogin() {
    this.navCtrl.setRoot(LoginPage)
  }

  optionUpload(photo) {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Profil Photo',
      buttons: [
        {
          text: 'Camera',
          role: 'destructive',
          icon: 'camera',
          handler: () => {
            this.takePicture(true, photo);
            console.log('Destructive clicked');
          }
        }, {
          text: 'Galery',
          icon: 'image',
          handler: () => {
            this.takePicture(false, photo);
            console.log('Archive clicked');
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(isCamera: boolean, photo: string) {
    const options: CameraOptions = {
      quality: 20,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: isCamera ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      if (photo == 'profil') {
        this.imageDataBase64 = 'data:image/jpeg;base64,' + imageData;
        console.log(this.imageDataBase64);
        this.imageData = imageData;
        const idx = this.photos.findIndex((item) => item.key == 'profil');
        if (idx != -1) {
          this.photos[idx].key = 'profil';
          this.photos[idx].file = this.imageData;
        }
        else {
          this.photos.push({ key: 'profil', file: this.imageData });
        }
        console.log(this.imageData);
      }
      else if (photo == 'photo1') {
        this.imageDataBase64_1 = 'data:image/jpeg;base64,' + imageData;
        console.log(this.imageDataBase64_1);
        this.imageData_1 = imageData;
        const idx = this.photos.findIndex((item) => item.key == 'pict1');
        if (idx != -1) {
          this.photos[idx].key = 'pict1';
          this.photos[idx].file = this.imageData_1;
        }
        else {
          this.photos.push({ key: 'pict1', file: this.imageData_1 });
        }
        console.log(this.imageData_1);

      }
      else if (photo == 'photo2') {
        this.imageDataBase64_2 = 'data:image/jpeg;base64,' + imageData;
        console.log(this.imageDataBase64_2);
        this.imageData_2 = imageData;
        const idx = this.photos.findIndex((item) => item.key == 'pict2');
        if (idx != -1) {
          this.photos[idx].key = 'pict2';
          this.photos[idx].file = this.imageData_2;
        }
        else {
          this.photos.push({ key: 'pict2', file: this.imageData_2 });
        }
        console.log(this.imageData_2);

      }
      else if (photo == 'photo3') {
        this.imageDataBase64_3 = 'data:image/jpeg;base64,' + imageData;
        console.log(this.imageDataBase64_3);
        this.imageData_3 = imageData;
        console.log(this.imageData_3);
        const idx = this.photos.findIndex((item) => item.key == 'pict3');
        if (idx != -1) {
          this.photos[idx].key = 'pict3';
          this.photos[idx].file = this.imageData_3;
        }
        else {
          this.photos.push({ key: 'pict3', file: this.imageData_3 });
        }
      }
      else {
        this.imageDataBase64_4 = 'data:image/jpeg;base64,' + imageData;
        console.log(this.imageDataBase64_4);
        this.imageData_4 = imageData;
        console.log(this.imageData_4);
        const idx = this.photos.findIndex((item) => item.key == 'pict4');
        if (idx != -1) {
          this.photos[idx].key = 'pict4';
          this.photos[idx].file = this.imageData_4;
        }
        else {
          this.photos.push({ key: 'pict4', file: this.imageData_4 });
        }
      }

    }, (err) => {
      // Handle error
    });
  }

  // gotoMaps(){
  //   this.navCtrl.push("MapsPage").then(() => {
  //     this.navCtrl.getActive().onDidDismiss((data) => {
  //       if(data) {
  //         console.log('lat',data.lat);
  //         console.log('long',data.lng);
  //         console.log('address',data.address);
  //       }
  //     })
  //   });
  // }

  openMaps() {
    const modal = this.modalCtrl.create("MapsPage");
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) {
        this.formData.latitude = data.lat;
        this.formData.longtitude = data.lng;
        this.formData.address = data.address;
      }
    })
  }

  nextSwitch(tab) {
    this.building = tab;
    this.content.scrollToTop();
  }
}
