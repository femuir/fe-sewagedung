import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateBuildingPage } from './update-building';

@NgModule({
  declarations: [
    UpdateBuildingPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateBuildingPage),
  ],
})
export class UpdateBuildingPageModule {}
