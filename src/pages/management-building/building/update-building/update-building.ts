import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, ToastController, AlertController, ActionSheetController, ModalController, ViewController, Content } from 'ionic-angular';
import { RequestRegisterBuildingDto, ApiProvider } from '../../../../providers/api/api';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { GlobalProvider } from '../../../../providers/global/global';
import { Storage } from '@ionic/storage';
import { UserProfilPage } from '../../../user-profil/user-profil';

/**
 * Generated class for the UpdateBuildingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-building',
  templateUrl: 'update-building.html',
})
export class UpdateBuildingPage {
  @ViewChild(Content) content: Content;
  building = 'detail';
  imageData: any;
  imageDataBase64: any;
  imageData_1: any;
  imageDataBase64_1: any;
  imageData_2: any;
  imageDataBase64_2: any;
  imageData_3: any;
  imageDataBase64_3: any;
  imageData_4: any;
  imageDataBase64_4: any;
  tNc: boolean = false;
  photos: Array<{ key: string, file: any }> = [];
  user: any;
  dataBuilding: any;
  srcImage: any;

  formData: RequestRegisterBuildingDto = {
    buildingId: '',
    buildingName: '',
    address: '',
    phone: '',
    capacity: '',
    price: '',
    moreInfo: '',
    status: 'N',
    managementId: '',
    city: '',
    bank: '',
    accountNumber: '',
    accountName: '',
    f1: '',
    f2: '',
    f3: '',
    f4: '',
    f5: '',
    f6: '',
    f7: '',
    f8: '',
    f9: '',
    f10: '',
    latitude: '',
    longtitude: ''
  }

  loading: Loading;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public alertCrtl: AlertController,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    public globalProvider: GlobalProvider,
    public storage: Storage,
    public modalCtrl: ModalController,
    public viewCrtl: ViewController,
  ) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateBuildingPage');
  }

  async getData() {
    try {
      //get data user from storage
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      if (this.user) {
        this.formData.managementId = this.user.userId;
      }
      //get data building from storage
      const building = await this.storage.get('building');
      this.dataBuilding = JSON.parse(building);
      if (this.dataBuilding) {
        this.formData.managementId = this.user.userId;
        this.formData.buildingId = this.dataBuilding.buildingId;
        this.formData.buildingName = this.dataBuilding.buildingName;
        this.formData.address = this.dataBuilding.address;
        this.formData.phone = this.dataBuilding.phone;
        this.formData.capacity = this.dataBuilding.capacity;
        this.formData.price = this.dataBuilding.price;
        this.formData.moreInfo = this.dataBuilding.moreInfo;
        this.formData.status = this.dataBuilding.status;
        this.formData.managementId = this.dataBuilding.managementId;
        this.formData.city = this.dataBuilding.city;
        this.formData.bank = this.dataBuilding.bank;
        this.formData.accountNumber = this.dataBuilding.accountNumber;
        this.formData.accountName = this.dataBuilding.accountName;
        this.formData.f1 = this.dataBuilding.f1;
        this.formData.f2 = this.dataBuilding.f2;
        this.formData.f3 = this.dataBuilding.f3;
        this.formData.f4 = this.dataBuilding.f4;
        this.formData.f5 = this.dataBuilding.f5;
        this.formData.f6 = this.dataBuilding.f6;
        this.formData.f7 = this.dataBuilding.f7;
        this.formData.f8 = this.dataBuilding.f8;
        this.formData.f9 = this.dataBuilding.f9;
        this.formData.f10 = this.dataBuilding.f10;
        this.formData.latitude = this.dataBuilding.latitude;
        this.formData.longtitude = this.dataBuilding.longtitude;
        this.srcImage = this.apiProvider.urlImage + 'Building/' + this.dataBuilding.photoId + '/';
        this.imageData = this.srcImage + this.dataBuilding.profil;
        this.imageData_1 = this.srcImage + this.dataBuilding.pict1;
        this.imageData_2 = this.srcImage + this.dataBuilding.pict2;
        this.imageData_3 = this.srcImage + this.dataBuilding.pict3;
        this.imageData_4 = this.srcImage + this.dataBuilding.pict4;

      }
      console.log('userId', this.formData.managementId);
    }
    catch (error) {
      console.log(error)
    }
  }

  doUpdate() {
    this.presentLoading(null);
    this.apiProvider.updateBuilding(this.formData)
      .subscribe(async (result: any) => {
        console.log(result);
        this.dismissLoading();

        try {
          if (this.photos.length > 0) {
            this.presentLoading('Upload Gambar...');
            await this.uploadMultiple(this.dataBuilding.photoId);
            this.dismissLoading();
          }
          this.showToast(result.msg);
          this.goUserProfil();
        }
        catch (error) {
          console.log(error);
          this.dismissLoading();
          this.showToast(error.error.msg);
        }
      }, error => {
        this.dismissLoading();
        // this.showAlert(error.error.msg);
        this.showToast(error.error.msg);
      })
  }

  async uploadMultiple(photoId) {
    return new Promise(async (resolve, reject) => {
      if (this.photos.length > 0) {
        try {
          for (let item of this.photos) {
            const filename = new Date().getTime().toString() + '_' + photoId + '.jpg';
            const image = await this.globalProvider.processFileFromUri(item.file);
            await this.upload(image, photoId, item.key, filename);
          }

          resolve(true);
        }
        catch (error) {
          reject(error);
        }

      }
    })
  }

  upload(file, photoId, key, filename) {
    return new Promise((resolve, reject) => {
      this.apiProvider.uploadPhotoBuilding(file, photoId, key, filename)
        .subscribe((result) => {
          resolve(result);
        }, error => {
          reject(error);
        })
    })
  }

  presentLoading(msg) {
    if (msg == null) {
      msg = "Mohon tunggu..."
    }
    this.loading = this.loadingCtrl.create({
      content: msg,
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  showAlert(message: string) {
    const alert = this.alertCrtl.create({
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }


  goUserProfil() {
    this.navCtrl.setRoot(UserProfilPage);
  }

  optionUpload(photo) {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Profil Photo',
      buttons: [
        {
          text: 'Camera',
          role: 'destructive',
          icon: 'camera',
          handler: () => {
            this.takePicture(true, photo);
            console.log('Destructive clicked');
          }
        }, {
          text: 'Galery',
          icon: 'image',
          handler: () => {
            this.takePicture(false, photo);
            console.log('Archive clicked');
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(isCamera: boolean, photo: string) {
    const options: CameraOptions = {
      quality: 20,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: isCamera ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      if (photo == 'profil') {
        this.imageDataBase64 = 'data:image/jpeg;base64,' + imageData;
        console.log(this.imageDataBase64);
        this.imageData = imageData;
        const idx = this.photos.findIndex((item) => item.key == 'profil');
        if (idx != -1) {
          this.photos[idx].key = 'profil';
          this.photos[idx].file = this.imageData;
        }
        else {
          this.photos.push({ key: 'profil', file: this.imageData });
        }
        console.log(this.imageData);
      }
      else if (photo == 'photo1') {
        this.imageDataBase64_1 = 'data:image/jpeg;base64,' + imageData;
        console.log(this.imageDataBase64_1);
        this.imageData_1 = imageData;
        const idx = this.photos.findIndex((item) => item.key == 'pict1');
        if (idx != -1) {
          this.photos[idx].key = 'pict1';
          this.photos[idx].file = this.imageData_1;
        }
        else {
          this.photos.push({ key: 'pict1', file: this.imageData_1 });
        }
        console.log(this.imageData_1);

      }
      else if (photo == 'photo2') {
        this.imageDataBase64_2 = 'data:image/jpeg;base64,' + imageData;
        console.log(this.imageDataBase64_2);
        this.imageData_2 = imageData;
        const idx = this.photos.findIndex((item) => item.key == 'pict2');
        if (idx != -1) {
          this.photos[idx].key = 'pict2';
          this.photos[idx].file = this.imageData_2;
        }
        else {
          this.photos.push({ key: 'pict2', file: this.imageData_2 });
        }
        console.log(this.imageData_2);

      }
      else if (photo == 'photo3') {
        this.imageDataBase64_3 = 'data:image/jpeg;base64,' + imageData;
        console.log(this.imageDataBase64_3);
        this.imageData_3 = imageData;
        console.log(this.imageData_3);
        const idx = this.photos.findIndex((item) => item.key == 'pict3');
        if (idx != -1) {
          this.photos[idx].key = 'pict3';
          this.photos[idx].file = this.imageData_3;
        }
        else {
          this.photos.push({ key: 'pict3', file: this.imageData_3 });
        }
      }
      else {
        this.imageDataBase64_4 = 'data:image/jpeg;base64,' + imageData;
        console.log(this.imageDataBase64_4);
        this.imageData_4 = imageData;
        console.log(this.imageData_4);
        const idx = this.photos.findIndex((item) => item.key == 'pict4');
        if (idx != -1) {
          this.photos[idx].key = 'pict4';
          this.photos[idx].file = this.imageData_4;
        }
        else {
          this.photos.push({ key: 'pict4', file: this.imageData_4 });
        }
      }

    }, (err) => {
      // Handle error
    });
  }

  // gotoMaps(){
  //   this.navCtrl.push("MapsPage").then(() => {
  //     this.navCtrl.getActive().onDidDismiss((data) => {
  //       if(data) {
  //         console.log('lat',data.lat);
  //         console.log('long',data.lng);
  //         console.log('address',data.address);
  //       }
  //     })
  //   });
  // }

  openMaps() {
    const modal = this.modalCtrl.create("MapsPage");
    modal.present();

    modal.onDidDismiss((data) => {
      if (data) {
        this.formData.latitude = data.lat;
        this.formData.longtitude = data.lng;
        this.formData.address = data.address;
      }
    })
  }

  dismiss() {
    this.viewCrtl.dismiss();
  }

  nextSwitch(tab) {
    this.building = tab;
    this.content.scrollToTop();
  }

}
