import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterManagementPage } from './register-management';

@NgModule({
  declarations: [
    RegisterManagementPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterManagementPage),
  ],
})
export class RegisterManagementPageModule {}
