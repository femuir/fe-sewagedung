import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, ToastController, ActionSheetController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { GlobalProvider } from '../../providers/global/global';
import { ApiProvider, RequestGetProfilDto } from '../../providers/api/api';
import { DetailPage } from '../gedung/detail/detail';

/**
 * Generated class for the UserProfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-profil',
  templateUrl: 'user-profil.html',
})
export class UserProfilPage {
  user: any;
  imageData: any;
  imageDataBase64: any;
  processing: any;
  uploadImage: any;
  loading: Loading;
  formData: RequestGetProfilDto = {
    email: ''
  }


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public storage: Storage,
    private camera: Camera,
    public globalProvider: GlobalProvider,
    public actionSheetCtrl: ActionSheetController,
    public modalCtrl: ModalController,

  ) {
  }

  async getUser() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      if (!this.user.picture) {
        this.imageData = "assets/imgs/default-profil.png";
      } else {
        this.imageData = this.apiProvider.urlImage + 'User/' + this.user.userId + '/' + this.user.picture;
      }
      //untuk update data di storage:
      // let temp = this.user;
      // temp.fullName = 'fer';
      // this.storage.set('user', JSON.stringify(temp))
      // console.log('temp', temp)
      console.log(user)
      console.log('userData', this.user)
      console.log(this.imageData);
    }
    catch (error) {
      console.log(error)
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserProfilPage');
    this.getUser();
  }

  goResetPassword() {
    this.navCtrl.push("ResetPasswordPage")
  }

  goUpdateProfile() {
    this.navCtrl.push("UpdateProfilePage")
  }

  goBuildingProfile(buildingId, role) {
    this.navCtrl.push(DetailPage, { buildingId, role })
  }

  logout() {
    this.presentLoading();
    this.storage.remove('user');
    this.dismissLoading();
    this.navCtrl.setRoot(LoginPage);
    this.showToast('Logout success');
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon Tunggu...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  optionUpload() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Profil Photo',
      buttons: [
        {
          text: 'Camera',
          role: 'destructive',
          icon: 'camera',
          handler: () => {
            this.takePicture(true);
            console.log('Destructive clicked');
          }
        }, {
          text: 'Galery',
          icon: 'image',
          handler: () => {
            this.takePicture(false);
            console.log('Archive clicked');
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(isCamera: boolean) {
    const options: CameraOptions = {
      quality: 20,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: isCamera ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.imageDataBase64 = 'data:image/jpeg;base64,' + imageData;
      console.log(this.imageDataBase64);
      this.imageData = imageData;
      console.log(this.imageData);
      this.uploadPhoto();
    }, (err) => {
      // Handle error
    });
  }

  async uploadPhoto() {
    const loading = this.loadingCtrl.create({ content: "Upload Foto Profil..."});
    const userId = this.user.userId;
    const time = new Date().getTime().toString();
    const fileName = `${userId + time}.jpg`
    console.log(fileName);
    loading.present();

    try {
      const image = await this.globalProvider.processFileFromUri(this.imageData);
      this.apiProvider.upload(image, userId, fileName)
        .subscribe((result: any) => {
          this.user.picture = fileName;
          this.storage.set('user', JSON.stringify(this.user))
          result.user = this.user;
          this.globalProvider.eventLogin.emit(result);
          loading.dismiss();
          this.showToast(result.msg);
        }, error => {
          console.log(error);
          loading.dismiss();
        })
    }
    catch (error) {
      console.log('çatch error', error);
      loading.dismiss();
    }
  }

  goViewPhoto(pathImage) {
    const modal = this.modalCtrl.create("ViewPhotoProfilPage", { photoProfil: pathImage });
    modal.present();
  }

  doRefresh(event) {
    // location.reload();
    this.formData.email = this.user.email;
    this.presentLoading();
    this.apiProvider.getProfil(this.formData)
      .subscribe((result: any) => {
        this.storage.set('user', JSON.stringify(result.user))
        console.log('cek storage', result.user);
        this.dismissLoading();
        this.ionViewDidLoad();
      }, error => {
        this.dismissLoading();
        this.showToast(error.error.msg);
      });

    setTimeout(() => {
      event.complete();
    }, 1000);
  }

  // doUploadPhoto() {
  //   const fileTransfer: FileTransferObject = this.transfer.create();

  //   let options: FileUploadOptions = {
  //     fileKey: 'picture',
  //     fileName: '22021996-1.jpg',
  //     headers: {},
  //     params: {
  //       'userId': '22021996'
  //     }
  //   }

  //   const loading = this.loadingCtrl.create({});
  //   loading.present();
  //   fileTransfer.upload(this.imageData, ' https://20d0a77f.ngrok.io/api/v1/user/updatePhoto', options)
  //     .then((data) => {
  //       // file:://documents/data/nama.jpg;
  //       // success
  //       console.log(data);
  //       loading.dismiss()
  //     }, (err) => {
  //       // error
  //       loading.dismiss()
  //       console.log(err);
  //     })
  // }
}
