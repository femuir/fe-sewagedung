import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { RequestUpdateProfileDto, ApiProvider } from '../../../providers/api/api';
import { UserProfilPage } from '../user-profil';
/**
 * Generated class for the UpdateProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-profile',
  templateUrl: 'update-profile.html',
})
export class UpdateProfilePage {
  user: any;

  formData: RequestUpdateProfileDto = {
    userId: '',
    fullName: '',
    gender: '',
    phone: '',
    address: ''
  }

  loading: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public alertCrtl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public storage: Storage
  ) 
  {
    this.getUser()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateProfilePage');
  }

  async getUser() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      this.formData.userId = this.user.userId;
      this.formData.fullName = this.user.fullName;
      this.formData.phone = this.user.phone;
      this.formData.address = this.user.address;
      console.log(user)
      
    }
    catch (error) {
      console.log(error)
    }
  }

  doUpdate() {
    this.presentLoading();
    this.apiProvider.updateProfile(this.formData)
      .subscribe((result: any) => {
        //start update storage
        let temp = this.user;
        temp.fullName = this.formData.fullName;
        temp.phone = this.formData.phone;
        temp.address = this.formData.address;
        temp.gender = this.formData.gender;
        this.storage.set('user', JSON.stringify(temp))
        console.log('temp', temp)
        //end update storage
        this.dismissLoading();
        this.showToast(result.msg);
        this.navCtrl.setRoot(UserProfilPage)  
      }, error => {
        this.dismissLoading();
        this.showToast(error.error.msg);
      })
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

}
