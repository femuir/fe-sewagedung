import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading, ToastController } from 'ionic-angular';
import { RequestResetPasswordDto, ApiProvider } from '../../../providers/api/api';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ResetPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {
  user: any;

  formData: RequestResetPasswordDto = {
    email: '',
    oldPassword: '',
    password: '',
    confirm_password: ''
  }

  loading: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public alertCrtl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public storage: Storage
  ) 
  {
    this.getUser()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordPage');
  }

  async getUser() {
    try {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      this.formData.email = this.user.email;
      console.log(user)
      console.log(this.formData.email)
      
    }
    catch (error) {
      console.log(error)
    }
  }

  doReset() {
    this.presentLoading();
    this.apiProvider.resetPassword(this.formData)
      .subscribe((result: any) => {
        this.dismissLoading();
        this.showToast(result.msg);
        this.navCtrl.pop();
      }, error => {
        this.dismissLoading();
        this.showToast(error.error.msg);
      })
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon tunggu...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

}
