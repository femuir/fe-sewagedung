import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewPhotoProfilPage } from './view-photo-profil';

@NgModule({
  declarations: [
    ViewPhotoProfilPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewPhotoProfilPage),
  ],
})
export class ViewPhotoProfilPageModule {}
