import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ApiProvider } from '../../../providers/api/api';

/**
 * Generated class for the ViewPhotoProfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-photo-profil',
  templateUrl: 'view-photo-profil.html',
})
export class ViewPhotoProfilPage {
  srcImage: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCrtl: ViewController,
    public apiProvider: ApiProvider,
    ) {
      this.srcImage = navParams.get('photoProfil');
      console.log(this.srcImage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyPaymentPage');
  }

  dismiss() {
    this.viewCrtl.dismiss();
  }

}
