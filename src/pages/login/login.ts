import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading, ToastController } from 'ionic-angular';
import { RequestLoginUserDto, ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { HomeUserPage } from '../home-user/home-user';
import { HomeManagementPage } from '../management-building/home-management/home-management';
import { GlobalProvider } from '../../providers/global/global';
import { RegisterBuildingPage } from '../management-building/building/register-building/register-building';
import { RegisterPage } from '../register/register';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  formData: RequestLoginUserDto = {
    email: '',
    password: ''
  }

  loading: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public storage: Storage,
    public globalProvider: GlobalProvider,
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  doLogin() {
    //validation input
    if (this.formData.email && this.formData.password) {
      this.presentLoading();
      this.apiProvider.loginUser(this.formData)
        .subscribe((result: any) => {
          this.setUserStorage(result);
          this.globalProvider.eventLogin.emit(result);
          //validation role user
          if (result.user.role == 2) {
            this.goHome();
          } else {
            if (result.user.buildingId == null) {
              const titleAlrt = "INPUT DATA GEDUNG!";
              const subTitleAlrt = "Anda terdaftar sebagai Pengelola Gedung, Maka WAJIB input data gedung yang akan disewakan";
              this.showAlert(titleAlrt, subTitleAlrt);
              this.goRegistBuilding();
            } else {
              this.goHomeManagement();
            }
          }
          this.dismissLoading();
          this.showToast(result.msg);

        }, error => {
          this.dismissLoading();
          this.showToast(error.error.msg);
        })
    } else if(!this.formData.email)
    {
      const titleAlrt = "EMAIL TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan email anda yang sudah teregistrasi";
      this.showAlert(titleAlrt, subTitleAlrt);
    } else if (!this.formData.password){
      const titleAlrt = "PASSWORD TIDAK BOLEH KOSONG!";
      const subTitleAlrt = "Silahkan masukan password anda";
      this.showAlert(titleAlrt, subTitleAlrt);
    }
  }

  async setUserStorage(result) {
    try {
      const user = await this.storage.set('user', JSON.stringify(result.user));
      console.log(user)
    }
    catch (error) {
      console.log(error)
    }
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon tunggu...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  showAlert(titleAlrt, subTitleAlrt) {
    const alert = this.alertCtrl.create({
      title: titleAlrt,
      subTitle: subTitleAlrt,
      buttons: ['OK']
    });
    alert.present();
  }

  goHome() {
    this.navCtrl.setRoot(HomeUserPage)
  }

  goHomeManagement() {
    this.navCtrl.setRoot(HomeManagementPage)
  }

  goRegistBuilding() {
    this.navCtrl.setRoot(RegisterBuildingPage)
  }

  goRegist() {
    this.navCtrl.push(RegisterPage)
  }
}
