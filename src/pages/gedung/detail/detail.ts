import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, ToastController } from 'ionic-angular';
import { ApiProvider, RequestListRateReviewDto, RequestListTrxDto } from '../../../providers/api/api';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';
import moment from 'moment';


/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {
  dgedung: string = "detail";
  dataBuilding: any = [];
  srcImage: any;
  srcImageUser: any;
  rateReview: any;
  dataBooking: any;
  listBookingDate: any;
  primaryImage: any;
  id: any;
  loading: Loading;
  latitude: any;
  longtitude: any;
  totalPage: number;
  totalPageBooking: number;
  httpError: boolean = false;
  locationError: boolean = false;

  formData: RequestListRateReviewDto = {
    page: 1,
    limit: 5,
    buildingId: ''
  }

  formDataBooking: RequestListTrxDto = {
    page: 1,
    limit: 5,
    userId: '',
    buildingId: '',
    status1: 'CHECKOUT',
    status2: 'PAYMENT',
    status3: 'BOOKING',
    status4: '',
    role: 2
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiProvider: ApiProvider,
    public loadingCtrl: LoadingController,
    private launchNavigator: LaunchNavigator,
    private geolocation: Geolocation,
    public toastCtrl: ToastController,
    public storage: Storage
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPage');
    this.id = this.navParams.get('buildingId');
    this.formDataBooking.role = this.navParams.get('role') || this.formDataBooking.role;
    console.log('buildingID', this.id, 'role:', this.formDataBooking.role);
    this.httpError = false;
    this.locationError = false;
    this.presentLoading();
    this.getDetailBuilding();
    this.getRateReview();
    this.getListBooking();
    this.getMyLocation();
  }

  getDetailBuilding() {
    this.apiProvider.detailBuilding(this.id)
      .subscribe((result: any) => {
        this.dataBuilding = result.building;
        if (this.dataBuilding.rate == null) {
          this.dataBuilding.rate = 0;
        }
        this.storage.set('building', JSON.stringify(result.building));
        this.srcImage = this.apiProvider.urlImage + 'Building/';
        this.primaryImage = this.dataBuilding.profil;
        console.log('buildingLat:', result.building.latitude, 'buildingLong', result.building.longtitude)
        // this.showAlert(result.message);
        // this.getMahasiswa();
      }, error => {
        console.log(error);
        this.httpError = true;
        this.dismissLoading();
      })
  }

  getMyLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longtitude = resp.coords.longitude;
      console.log('userLat', this.latitude, 'userLong', this.longtitude);

    }).catch((error) => {
      this.httpError = true;
      this.locationError = true;
      console.log('Error getting location', error);
    });
  }

  launchRute() {
    let options: LaunchNavigatorOptions = {
      start: `${this.latitude},${this.longtitude}`
    };

    this.launchNavigator.navigate(`${this.dataBuilding.latitude},${this.dataBuilding.longtitude}` , options)
    // this.launchNavigator.navigate(this.dataBuilding.address, options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
    console.log('destination: ', this.dataBuilding.buildingName, 'address: ', this.dataBuilding.address);
  }

  getRateReview() {
    this.formData.buildingId = this.id;
    this.apiProvider.listRateReview(this.formData)
      .subscribe((result: any) => {
        this.rateReview = result.rate.data;
        this.srcImageUser = this.apiProvider.urlImage + 'User/';
        this.totalPage = result.rate.totalPage;
      }, error => {
        this.dismissLoading();
        this.httpError = true;
        console.log(error);
        this.showToast(error.error.msg);
      })
  }

  doInfinite(infiniteScroll) {
    this.formData.page = this.formData.page + 1;
    this.apiProvider.listRateReview(this.formData)
      .subscribe((result: any) => {
        const data = result.rate.data;
        if (data.length > 0) {
          for (let item of data) {
            this.rateReview.push(item);
          }
        }
        infiniteScroll.complete();
      }, error => {
        infiniteScroll.complete();
      });
  }

  getListBooking() {
    this.formDataBooking.buildingId = this.id;
    this.apiProvider.listTransaction(this.formDataBooking)
      .subscribe((result: any) => {
        this.dataBooking = result.transaction.data;
        this.totalPageBooking = result.transaction.totalPage;
        this.dismissLoading();
      }, error => {
        this.dismissLoading();
        console.log(error);
        this.httpError = true;
        this.showToast(error.error.msg);
      })
  }

  doInfiniteBooking(infiniteScroll) {
    this.formDataBooking.page = this.formDataBooking.page + 1;
    this.apiProvider.listTransaction(this.formDataBooking)
      .subscribe((result: any) => {
        const data = result.transaction.data;
        if (data.length > 0) {
          for (let item of data) {
            this.dataBooking.push(item);
          }
        }
        infiniteScroll.complete();
      }, error => {
        infiniteScroll.complete();
      });
  }

  goCheckout() {
    this.navCtrl.push("CheckoutTransactionPage");
  }

  goUpdateBuilding() {
    this.navCtrl.push("UpdateBuildingPage");
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Mohon tunggu...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg: string) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  doRefresh(event) {
    this.ionViewDidLoad();
    setTimeout(() => {
      event.complete();
    }, 1000);
  }

  formatDate(date) {
    const result = moment(date).format('DD-MM-YYYY');

    return result;
  }
}
