import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiProvider } from '../providers/api/api';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { ListTransactionPage } from '../pages/transaction/list-transaction/list-transaction';
import { HomeUserPage } from '../pages/home-user/home-user';
import { StarRatingModule } from 'ionic3-star-rating';
import { UploadPaymentPage } from '../pages/transaction/upload-payment/upload-payment';
import { Camera } from '@ionic-native/camera';
import { GlobalProvider } from '../providers/global/global';
import { HomeManagementPage } from '../pages/management-building/home-management/home-management';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { Geolocation } from '@ionic-native/geolocation';
import { TabsPage } from '../pages/tabs/tabs';
import { UserRateModalPage } from '../pages/transaction/user-rate-modal/user-rate-modal';
import { DetailPage } from '../pages/gedung/detail/detail';
import { RegisterBuildingPage } from '../pages/management-building/building/register-building/register-building';
import { HelpCenterPage } from '../pages/help-center/help-center';
import { CallNumber } from '@ionic-native/call-number';
import { UserProfilPage } from '../pages/user-profil/user-profil';
import { LoginPage } from '../pages/login/login';
import { RejectTransactionPage } from '../pages/management-building/reject-transaction/reject-transaction';
import { RegisterPage } from '../pages/register/register';

@NgModule({
  declarations: [
    MyApp,
    UserProfilPage,
    ListTransactionPage,
    HomeUserPage,
    UploadPaymentPage,
    HomeManagementPage,
    TabsPage,
    UserRateModalPage,
    DetailPage,
    RegisterBuildingPage,
    HelpCenterPage,
    LoginPage,
    RejectTransactionPage,
    RegisterPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot(),
    StarRatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    UserProfilPage,
    ListTransactionPage,
    HomeUserPage,
    UploadPaymentPage,
    HomeManagementPage,
    TabsPage,
    UserRateModalPage,
    DetailPage,
    RegisterBuildingPage,
    HelpCenterPage,
    LoginPage,
    RejectTransactionPage,
    RegisterPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiProvider,
    Camera,
    GlobalProvider,
    FileTransfer,
    File,
    LaunchNavigator,
    Geolocation,
    CallNumber
  ]
})
export class AppModule { }
