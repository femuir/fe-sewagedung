import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, Loading, MenuController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserProfilPage } from '../pages/user-profil/user-profil';
import { Storage } from '@ionic/storage';
import { ListTransactionPage } from '../pages/transaction/list-transaction/list-transaction';
import { HomeUserPage } from '../pages/home-user/home-user';
import { HomeManagementPage } from '../pages/management-building/home-management/home-management';
import { GlobalProvider } from '../providers/global/global';
import { RegisterBuildingPage } from '../pages/management-building/building/register-building/register-building';
import { HelpCenterPage } from '../pages/help-center/help-center';
import { ApiProvider } from '../providers/api/api';
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  user: any;
  imageData: any;
  loading: Loading;

  pages: Array<{ title: string, component: any, icon: any }>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public storage: Storage,
    public globalProvider: GlobalProvider,
    public apiProvider: ApiProvider,
    public loadingCtrl: LoadingController,
    public menuController: MenuController,
    public toastCtrl: ToastController,
  ) {
    this.initializeApp();
    this.splashScreen.hide();
    this.initMenu();

    setInterval(async () => {
      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      if (this.user) {
        this.apiProvider.getNotification(this.user.userId)
          .subscribe((result: any) => {
            this.globalProvider.notif = result.notification;
            console.log('getNotification', result.notification);

          }, error => {
            console.log('error get notification', error);
          })
      }
    }, 5000)

    // used for an example of ngFor and navigation
    this.pages = [
      {
        title: 'Beranda',
        component: "HomePage",
        icon: 'ios-home'
      },
      {
        title: 'List',
        component: "ListPage",
        icon: 'md-list'
      },
      {
        title: 'Profil',
        component: "UserProfilPage",
        icon: 'md-people'
      },
      {
        title: 'Transaksi',
        component: "ListTransactionPage",
        icon: 'logo-buffer'
      },
    ];

  }

  initializeApp() {
    this.platform.ready().then(async () => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      const user = await this.storage.get('user');
      this.user = JSON.parse(user);
      if (this.user) {
        this.globalProvider.userName = this.user.fullName;
        if (this.user.role == 2) {
          this.nav.setRoot(HomeUserPage);
          this.pages = [
            {
              title: 'Beranda',
              component: HomeUserPage,
              icon: 'ios-home'
            },
            {
              title: 'Profil',
              component: UserProfilPage,
              icon: 'md-people'
            },
            {
              title: 'Transaksi',
              component: ListTransactionPage,
              icon: 'logo-buffer'
            },
            {
              title: 'Pusat Bantuan',
              component: HelpCenterPage,
              icon: 'md-help'
            }
          ];
        } else {
          if (this.user.buildingId == null) {
            // this.nav.setRoot(HomeManagementPage);
            this.nav.setRoot(RegisterBuildingPage);
          } else {
            this.nav.setRoot(HomeManagementPage);
          }
          this.pages = [
            {
              title: 'Beranda',
              component: HomeManagementPage,
              icon: 'ios-home'
            },
            {
              title: 'Profil',
              component: UserProfilPage,
              icon: 'md-people'
            },
            {
              title: 'Pusat Bantuan',
              component: HelpCenterPage,
              icon: 'md-help'
            }
          ];
        }
        if (this.user.picture) {
          const picture = this.apiProvider.urlImage + 'User/' + this.user.userId + '/' + this.user.picture;
          this.imageData = this.globalProvider.pathImage(picture);
        } else {
          this.imageData = "assets/imgs/default-profil.png";
        }
      }
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  initMenu() {
    this.globalProvider.eventLogin.subscribe((result: any) => {
      this.menuController.enable(true);
      if (result.user.picture) {
        const picture = this.apiProvider.urlImage + 'User/' + result.user.userId + '/' + result.user.picture;
        this.imageData = this.globalProvider.pathImage(picture);
      } else {
        this.imageData = "assets/imgs/default-profil.png";
      }
      this.globalProvider.userName = result.user.fullName;
      if (result.user.role == 2) {
        this.pages = [
          {
            title: 'Beranda',
            component: HomeUserPage,
            icon: 'ios-home'
          },
          {
            title: 'Profil',
            component: UserProfilPage,
            icon: 'md-people'
          },
          {
            title: 'Transaksi',
            component: ListTransactionPage,
            icon: 'logo-buffer'
          },
          {
            title: 'Pusat Bantuan',
            component: HelpCenterPage,
            icon: 'md-help'
          }
        ];

      } else {
        this.pages = [
          {
            title: 'Beranda',
            component: HomeManagementPage,
            icon: 'ios-home'
          },
          {
            title: 'Profil',
            component: UserProfilPage,
            icon: 'md-people'
          },
          {
            title: 'Pusat Bantuan',
            component: HelpCenterPage,
            icon: 'md-help'
          }
        ];
      }
    })
  }

  logout() {
    this.presentLoading();
    this.storage.remove('user');
    this.dismissLoading();
    this.nav.setRoot(LoginPage);
    this.menuController.enable(false);
    this.showToast('Logout success');
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
    });
    this.loading.present();
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  showToast(msg) {
    const toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }
}
