import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { Platform } from 'ionic-angular';
import { FileEntry, File } from '@ionic-native/file';

/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalProvider {
  public window: any = window;
  eventLogin: EventEmitter<any> = new EventEmitter();
  notif: any;
  userName: any;
  constructor(public http: HttpClient, public platform: Platform, public file: File) {
    console.log('Hello GlobalProvider Provider');
  }

  pathImage(image) {
    if (this.platform.is('cordova')) {
      // handle new version wkWebView version >= 3
      try {
        return this.window.Ionic.WebView.convertFileSrc(image);
      } catch (error) {
        return image;
      }
    }
    else {
      return image;
    }
  }

  readFile(file: any): Promise<any> {
    return new Promise<any>(resolve => {
      const reader = new FileReader();
      reader.onloadend = () => {
        const imgBlob = new Blob([reader.result], { type: file.type });
        resolve(imgBlob);
      };
      reader.readAsArrayBuffer(file);
    })
  }

  processFileFromUri(imageFileUri: any): Promise<any> { // get blob
    return new Promise<any>((resolve, reject) => {
      this.file.resolveLocalFilesystemUrl(imageFileUri)
        .then(entry => (<FileEntry>entry).file(async file => {
          const blob = await this.readFile(file);
          resolve(blob);
        }))
        .catch(err => {
          console.log(err);
          reject(err);
        }
        );
    })
  }


}
