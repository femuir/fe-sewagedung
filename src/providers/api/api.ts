import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Time } from '@angular/common';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
  //url local
  // url = "http://127.0.0.1:8000/api/v1";
  // urlImage = "http://127.0.0.1:8000/";
  //url public
  url = "http://panman.my.id/sewa-gedung/api/v1";
  urlImage = "http://panman.my.id/sewa-gedung/";

  // USER, begin
  constructor(public http: HttpClient) {
    console.log('Hello ApiProvider Provider');
  }

  loginUser(formData: RequestLoginUserDto) {
    return this.http.post(this.url + '/login', formData);
  }

  getProfil(formData: RequestGetProfilDto) {
    return this.http.post(this.url + '/user/getProfil', formData);
  }

  registerUser(formData: RequestRegisterUserDto) {
    return this.http.post(this.url + '/register', formData);
  }

  resetPassword(formData: RequestResetPasswordDto) {
    return this.http.post(this.url + '/user/resetPassword', formData);
  }

  updateProfile(formData: RequestUpdateProfileDto) {
    return this.http.post(this.url + '/user/updateProfil', formData);
  }

  upload(image: Blob, userId: any, filename) {
    let formData = new FormData();
    formData.append('picture', image, filename);
    formData.append('userId', userId);
    return this.http.post(this.url + '/user/updatePhoto', formData);
  }

  listBuilding(formData: RequestListBuildingDto) {
    return this.http.post(this.url + '/building/getListBuilding', formData);
  }

  detailBuilding(buildingId){
    return this.http.get(this.url + '/building/detailBuilding/' + buildingId);
  }

  listRateReview(formData: RequestListRateReviewDto) {
    return this.http.post(this.url + '/transaction/listRate', formData);
  }

  listTransaction(formData: RequestListTrxDto) {
    return this.http.post(this.url + '/transaction/listTrx', formData);
  }

  checkoutTransaction(formData: RequestCheckoutTrxgDto) {
    return this.http.post(this.url + '/transaction/checkout', formData);
  }

  detailTransaction(formData: RequestDetailTrxgDto) {
    return this.http.post(this.url + '/transaction/detailTrx', formData);
  }

  uploadPayment(image: Blob, trxId: any, filename) {
    let formData = new FormData();
    formData.append('payment', image, filename);
    formData.append('trnId', trxId);
    formData.append('status', 'DIBAYAR');
    formData.append('updatedBy', 'USER');
    return this.http.post(this.url + '/transaction/uploadPayment', formData);
  }

  updateTransaction(formData: RequestUpdateTrxgDto) {
    return this.http.post(this.url + '/transaction/updateTrx', formData);
  }

  inputRateReview(formData: RequestInputRateDto) {
    return this.http.post(this.url + '/transaction/inputRate', formData);
  }
  // USER, end

  // Management, begin
  registerBuilding(formData: RequestRegisterBuildingDto) {
    return this.http.post(this.url + '/building/inputBuilding', formData);
  }

  updateBuilding(formData: RequestRegisterBuildingDto) {
    return this.http.post(this.url + '/building/updateBuilding', formData);
  }

  uploadPhotoBuilding(image: Blob, photoId: any, picture:any, filename) {
    let formData = new FormData();
    formData.append(picture, image, filename);
    formData.append('photoId', photoId);
    return this.http.post(this.url + '/building/updatePhoto', formData);
  }
  // Management, end

  //notification, begin
  getNotification(to){
    return this.http.get(this.url + '/notification/getNotification/' + to);
  }

  updateNotification(id){
    return this.http.get(this.url + '/notification/updateNotification/' + id);
  }
  //notification, end

 
}

export interface RequestLoginUserDto {
  email: string,
  password: string
}

export interface RequestRegisterUserDto {
  userId: string,
  fullName: string,
  gender: string,
  phone: string,
  address: string,
  role: number,
  email: string,
  password: string,
  confirm_password: string
}

export interface RequestGetProfilDto {
  email: string,
}

export interface RequestResetPasswordDto {
  email: string,
  oldPassword: string,
  password: string,
  confirm_password: string
}

export interface RequestUpdateProfileDto {
  userId: string,
  fullName: string,
  gender: string,
  phone: string,
  address: string
}

export interface RequestListBuildingDto {
  page: number,
  limit: number,
  search: string,
  price: number,
  rating: number,
  capacity: number,
  sortBy: string,
  lat1: number,
  long1: number
}

export interface RequestListRateReviewDto {
  page: number,
  limit: number,
  buildingId: string
}

export interface RequestListTrxDto {
  page: number,
  limit: number,
  userId: string,
  buildingId: string,
  status1: string,
  status2: string,
  status3: string,
  status4: string,
  role: number
}

export interface RequestCheckoutTrxgDto {
  buildingId: string,
  userId: string,
  date: Date,
  time: Time,
  notes: string
}

export interface RequestDetailTrxgDto {
  trnId: string,
  buildingId: string
}

export interface RequestUpdateTrxgDto {
  trnId: string,
  status: string,
  notes: string,
  updatedBy: string
}

export interface RequestInputRateDto {
  buildingId: string,
  userId: string,
  trnId: string,
  rate: number,
  comment: string,
  rated: number
}

// Management Building
export interface RequestRegisterBuildingDto {
  buildingId: string,
  buildingName: string,
  address: string,
  phone: string,
  capacity: string,
  status: string,
  managementId: string,
  city: string,
  price: string,
  bank: string,
  accountNumber: string,
  accountName: string,
  moreInfo: string,
  f1: string,
  f2: string,
  f3: string,
  f4: string,
  f5: string,
  f6: string,
  f7: string,
  f8: string,
  f9: string,
  f10: string,
  latitude: string,
  longtitude: string
}
